import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../../common/failure.dart';
import '../../domain/models/weather_model.dart';
import '../../domain/repositories/fetch_weather_repositories.dart';
import '../source/fetch_weather_source.dart';

class FetchWeatherRepositoryImpl implements FetchWeatherRepository {
  final FetchWeatherSource source;
  FetchWeatherRepositoryImpl({required this.source});
  @override
  Future<Either<Failure, CityWeatherModel>> fetchData(String cityName) async {
    try {
      var response = await source.fetchWeather(cityName);
      return Right(response);
    } on DioException catch (e) {
      return Left(ResponseFailure.fromResponse(e.response ?? e));
    } on SocketException catch (e) {
      return Left(ResponseFailure.fromResponse(e));
    } catch (e) {
      return Left(ResponseFailure.fromResponse(e));
    }
  }
}
