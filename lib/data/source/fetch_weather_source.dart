import 'package:dio/dio.dart';
import 'package:potok_test/domain/models/weather_model.dart';
import 'package:retrofit/http.dart';

import '../../common/api.dart';

part 'fetch_weather_source.g.dart';

@RestApi(baseUrl: Api.host)
abstract class FetchWeatherSource {
  factory FetchWeatherSource(Dio dio, {String? baseUrl}) = _FetchWeatherSource;

  @GET(Api.fetchData)
  Future<CityWeatherModel> fetchWeather(@Query('q') String city);
}
