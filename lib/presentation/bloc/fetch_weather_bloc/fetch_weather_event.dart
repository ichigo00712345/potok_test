part of 'fetch_weather_bloc.dart';

@freezed
class FetchWeatherEvent with _$FetchWeatherEvent {
  const factory FetchWeatherEvent.fetchWeather() = FetchWeather;
}
