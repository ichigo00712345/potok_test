import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:potok_test/domain/useceses/fetch_weather_useceses.dart';

import '../../../common/mocked_city_list.dart';
import '../../../domain/models/weather_model.dart';

part 'fetch_weather_bloc.freezed.dart';
part 'fetch_weather_event.dart';
part 'fetch_weather_state.dart';

class FetchWeatherBloc extends Bloc<FetchWeatherEvent, FetchWeatherState> {
  final FetchWeatherUsecases usecases;
  FetchWeatherBloc(this.usecases) : super(const _Initial()) {
    on<FetchWeather>((event, emit) async {
      emit(const Loading());
      final int index = Random().nextInt(3);
      final String cityName = mockedCityList[index];
      final response = await usecases.fetchWeather(cityName);
      response.fold((l) {
        emit(Error(l.message));
      }, (r) {
        emit(Success(r));
      });
    });
  }
}
