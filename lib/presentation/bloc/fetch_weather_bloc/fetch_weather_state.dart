part of 'fetch_weather_bloc.dart';

@freezed
class FetchWeatherState with _$FetchWeatherState {
  const factory FetchWeatherState.initial() = _Initial;
  const factory FetchWeatherState.loading() = Loading;
  const factory FetchWeatherState.success(CityWeatherModel weatherData) = Success;
  const factory FetchWeatherState.error(String error) = Error;
}
