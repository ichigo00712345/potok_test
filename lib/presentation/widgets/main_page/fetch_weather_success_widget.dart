import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/models/weather_model.dart';
import '../../bloc/fetch_weather_bloc/fetch_weather_bloc.dart';

class FetchWeatherSuccessWidget extends StatelessWidget {
  final CityWeatherModel weatherData;
  const FetchWeatherSuccessWidget({super.key, required this.weatherData});

  @override
  Widget build(BuildContext context) {
    final String iconUrl = 'https://openweathermap.org/img/wn/${weatherData.weather?[0].icon}@2x.png';
    final String temperature = "${weatherData.main?.temp.toString() ?? '0'} °C";
    final String city = weatherData.name ?? 'No data';
    final String windSpeed = 'Speed: ${weatherData.wind?.speed} m/s';
    final String windDeg = 'Deg: ${weatherData.wind?.deg}°';

    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            city,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Image.network(
                    iconUrl,
                    width: 100,
                    height: 100,
                  ),
                  const SizedBox(width: 10),
                  Text(
                    temperature,
                  )
                ],
              ),
              const SizedBox(width: 30),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Wind:'),
                  Text(windSpeed),
                  Text(windDeg),
                ],
              )
            ],
          ),
          const SizedBox(height: 100),
          ElevatedButton(
              onPressed: () {
                context.read<FetchWeatherBloc>().add(const FetchWeather());
              },
              child: const Text('Fetch new weather'))
        ],
      ),
    );
  }
}
