import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/fetch_weather_bloc/fetch_weather_bloc.dart';

class FetchWeatherErrorWidget extends StatelessWidget {
  final String errorMessage;
  const FetchWeatherErrorWidget({super.key, required this.errorMessage});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              errorMessage,
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 100),
          ElevatedButton(
              onPressed: () {
                context.read<FetchWeatherBloc>().add(const FetchWeather());
              },
              child: const Text('Fetch new weather'))
        ],
      ),
    );
  }
}
