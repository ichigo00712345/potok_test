import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common/get_it.dart' as get_it;
import '../../../domain/useceses/fetch_weather_useceses.dart' hide FetchWeather;
import '../../bloc/fetch_weather_bloc/fetch_weather_bloc.dart';
import 'fetch_weather_error_widget.dart';
import 'fetch_weather_success_widget.dart';

class MainPageContent extends StatelessWidget {
  const MainPageContent({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FetchWeatherBloc(get_it.sl.get<FetchWeatherUsecases>()),
      child: BlocBuilder<FetchWeatherBloc, FetchWeatherState>(
        builder: (context, state) {
          final bloc = context.read<FetchWeatherBloc>();
          if (state is Loading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is Success) {
            return FetchWeatherSuccessWidget(
              weatherData: state.weatherData,
            );
          } else if (state is Error) {
            return FetchWeatherErrorWidget(
              errorMessage: state.error,
            );
          }
          return Center(
            child: ElevatedButton(
              onPressed: () {
                bloc.add(const FetchWeather());
              },
              child: const Text('Show random city weather'),
            ),
          );
        },
      ),
    );
  }
}
