import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:potok_test/data/repositories/fetch_weather_repository_impl.dart';
import 'package:potok_test/data/source/fetch_weather_source.dart';
import 'package:potok_test/domain/repositories/fetch_weather_repositories.dart';
import 'package:potok_test/domain/useceses/fetch_weather_useceses.dart';

import '../presentation/bloc/fetch_weather_bloc/fetch_weather_bloc.dart';

final sl = GetIt.instance;

init() async {
  //auth
  sl.registerFactory(() => FetchWeatherBloc(sl()));
  sl.registerLazySingleton(() => FetchWeatherUsecases(sl()));
  sl.registerLazySingleton<FetchWeatherRepository>(() => FetchWeatherRepositoryImpl(source: sl()));
  sl.registerLazySingleton<FetchWeatherSource>(() => FetchWeatherSource(sl()));

  // dio
  sl.registerLazySingleton(() => Dio());
}
