abstract class Api {
  static const _apiKey = "f158cf828b0071b1a92a76e6fde358de";
  static const host = "https://api.openweathermap.org/data/2.5";
  static const fetchData = "/weather?appid=$_apiKey&units=metric";
}
