import 'package:dartz/dartz.dart';

import '../../common/failure.dart';
import '../models/weather_model.dart';

abstract class FetchWeatherRepository {
  Future<Either<Failure, CityWeatherModel>> fetchData(String cityName);
}
