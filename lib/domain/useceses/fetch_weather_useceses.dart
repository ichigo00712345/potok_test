import 'package:dartz/dartz.dart';
import 'package:potok_test/domain/models/weather_model.dart';
import 'package:potok_test/domain/repositories/fetch_weather_repositories.dart';

import '../../common/failure.dart';
import '../../common/usecases.dart';

class FetchWeather extends UseCase<CityWeatherModel, String> {
  final FetchWeatherRepository repository;

  FetchWeather(this.repository);

  @override
  Future<Either<Failure, CityWeatherModel>> call(params) async {
    return await repository.fetchData(params);
  }
}

class FetchWeatherUsecases {
  final FetchWeatherRepository fetchWeatherRepository;
  late FetchWeather fetchWeather;

  FetchWeatherUsecases(this.fetchWeatherRepository) {
    fetchWeather = FetchWeather(fetchWeatherRepository);
  }
}
