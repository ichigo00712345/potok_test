// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'weather_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CityWeatherModel _$CityWeatherModelFromJson(Map<String, dynamic> json) {
  return _CityWeatherModel.fromJson(json);
}

/// @nodoc
mixin _$CityWeatherModel {
  List<WeatherModel>? get weather => throw _privateConstructorUsedError;
  MainModel? get main => throw _privateConstructorUsedError;
  WindModel? get wind => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CityWeatherModelCopyWith<CityWeatherModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CityWeatherModelCopyWith<$Res> {
  factory $CityWeatherModelCopyWith(
          CityWeatherModel value, $Res Function(CityWeatherModel) then) =
      _$CityWeatherModelCopyWithImpl<$Res, CityWeatherModel>;
  @useResult
  $Res call(
      {List<WeatherModel>? weather,
      MainModel? main,
      WindModel? wind,
      String? name});

  $MainModelCopyWith<$Res>? get main;
  $WindModelCopyWith<$Res>? get wind;
}

/// @nodoc
class _$CityWeatherModelCopyWithImpl<$Res, $Val extends CityWeatherModel>
    implements $CityWeatherModelCopyWith<$Res> {
  _$CityWeatherModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? weather = freezed,
    Object? main = freezed,
    Object? wind = freezed,
    Object? name = freezed,
  }) {
    return _then(_value.copyWith(
      weather: freezed == weather
          ? _value.weather
          : weather // ignore: cast_nullable_to_non_nullable
              as List<WeatherModel>?,
      main: freezed == main
          ? _value.main
          : main // ignore: cast_nullable_to_non_nullable
              as MainModel?,
      wind: freezed == wind
          ? _value.wind
          : wind // ignore: cast_nullable_to_non_nullable
              as WindModel?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $MainModelCopyWith<$Res>? get main {
    if (_value.main == null) {
      return null;
    }

    return $MainModelCopyWith<$Res>(_value.main!, (value) {
      return _then(_value.copyWith(main: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $WindModelCopyWith<$Res>? get wind {
    if (_value.wind == null) {
      return null;
    }

    return $WindModelCopyWith<$Res>(_value.wind!, (value) {
      return _then(_value.copyWith(wind: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_CityWeatherModelCopyWith<$Res>
    implements $CityWeatherModelCopyWith<$Res> {
  factory _$$_CityWeatherModelCopyWith(
          _$_CityWeatherModel value, $Res Function(_$_CityWeatherModel) then) =
      __$$_CityWeatherModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<WeatherModel>? weather,
      MainModel? main,
      WindModel? wind,
      String? name});

  @override
  $MainModelCopyWith<$Res>? get main;
  @override
  $WindModelCopyWith<$Res>? get wind;
}

/// @nodoc
class __$$_CityWeatherModelCopyWithImpl<$Res>
    extends _$CityWeatherModelCopyWithImpl<$Res, _$_CityWeatherModel>
    implements _$$_CityWeatherModelCopyWith<$Res> {
  __$$_CityWeatherModelCopyWithImpl(
      _$_CityWeatherModel _value, $Res Function(_$_CityWeatherModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? weather = freezed,
    Object? main = freezed,
    Object? wind = freezed,
    Object? name = freezed,
  }) {
    return _then(_$_CityWeatherModel(
      weather: freezed == weather
          ? _value._weather
          : weather // ignore: cast_nullable_to_non_nullable
              as List<WeatherModel>?,
      main: freezed == main
          ? _value.main
          : main // ignore: cast_nullable_to_non_nullable
              as MainModel?,
      wind: freezed == wind
          ? _value.wind
          : wind // ignore: cast_nullable_to_non_nullable
              as WindModel?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CityWeatherModel implements _CityWeatherModel {
  _$_CityWeatherModel(
      {final List<WeatherModel>? weather, this.main, this.wind, this.name})
      : _weather = weather;

  factory _$_CityWeatherModel.fromJson(Map<String, dynamic> json) =>
      _$$_CityWeatherModelFromJson(json);

  final List<WeatherModel>? _weather;
  @override
  List<WeatherModel>? get weather {
    final value = _weather;
    if (value == null) return null;
    if (_weather is EqualUnmodifiableListView) return _weather;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final MainModel? main;
  @override
  final WindModel? wind;
  @override
  final String? name;

  @override
  String toString() {
    return 'CityWeatherModel(weather: $weather, main: $main, wind: $wind, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CityWeatherModel &&
            const DeepCollectionEquality().equals(other._weather, _weather) &&
            (identical(other.main, main) || other.main == main) &&
            (identical(other.wind, wind) || other.wind == wind) &&
            (identical(other.name, name) || other.name == name));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(_weather), main, wind, name);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CityWeatherModelCopyWith<_$_CityWeatherModel> get copyWith =>
      __$$_CityWeatherModelCopyWithImpl<_$_CityWeatherModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CityWeatherModelToJson(
      this,
    );
  }
}

abstract class _CityWeatherModel implements CityWeatherModel {
  factory _CityWeatherModel(
      {final List<WeatherModel>? weather,
      final MainModel? main,
      final WindModel? wind,
      final String? name}) = _$_CityWeatherModel;

  factory _CityWeatherModel.fromJson(Map<String, dynamic> json) =
      _$_CityWeatherModel.fromJson;

  @override
  List<WeatherModel>? get weather;
  @override
  MainModel? get main;
  @override
  WindModel? get wind;
  @override
  String? get name;
  @override
  @JsonKey(ignore: true)
  _$$_CityWeatherModelCopyWith<_$_CityWeatherModel> get copyWith =>
      throw _privateConstructorUsedError;
}

WeatherModel _$WeatherModelFromJson(Map<String, dynamic> json) {
  return _WeatherModel.fromJson(json);
}

/// @nodoc
mixin _$WeatherModel {
  String? get main => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  String? get icon => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WeatherModelCopyWith<WeatherModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WeatherModelCopyWith<$Res> {
  factory $WeatherModelCopyWith(
          WeatherModel value, $Res Function(WeatherModel) then) =
      _$WeatherModelCopyWithImpl<$Res, WeatherModel>;
  @useResult
  $Res call({String? main, String? description, String? icon});
}

/// @nodoc
class _$WeatherModelCopyWithImpl<$Res, $Val extends WeatherModel>
    implements $WeatherModelCopyWith<$Res> {
  _$WeatherModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? main = freezed,
    Object? description = freezed,
    Object? icon = freezed,
  }) {
    return _then(_value.copyWith(
      main: freezed == main
          ? _value.main
          : main // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      icon: freezed == icon
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_WeatherModelCopyWith<$Res>
    implements $WeatherModelCopyWith<$Res> {
  factory _$$_WeatherModelCopyWith(
          _$_WeatherModel value, $Res Function(_$_WeatherModel) then) =
      __$$_WeatherModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? main, String? description, String? icon});
}

/// @nodoc
class __$$_WeatherModelCopyWithImpl<$Res>
    extends _$WeatherModelCopyWithImpl<$Res, _$_WeatherModel>
    implements _$$_WeatherModelCopyWith<$Res> {
  __$$_WeatherModelCopyWithImpl(
      _$_WeatherModel _value, $Res Function(_$_WeatherModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? main = freezed,
    Object? description = freezed,
    Object? icon = freezed,
  }) {
    return _then(_$_WeatherModel(
      main: freezed == main
          ? _value.main
          : main // ignore: cast_nullable_to_non_nullable
              as String?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      icon: freezed == icon
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_WeatherModel implements _WeatherModel {
  _$_WeatherModel({this.main, this.description, this.icon});

  factory _$_WeatherModel.fromJson(Map<String, dynamic> json) =>
      _$$_WeatherModelFromJson(json);

  @override
  final String? main;
  @override
  final String? description;
  @override
  final String? icon;

  @override
  String toString() {
    return 'WeatherModel(main: $main, description: $description, icon: $icon)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WeatherModel &&
            (identical(other.main, main) || other.main == main) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.icon, icon) || other.icon == icon));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, main, description, icon);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WeatherModelCopyWith<_$_WeatherModel> get copyWith =>
      __$$_WeatherModelCopyWithImpl<_$_WeatherModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WeatherModelToJson(
      this,
    );
  }
}

abstract class _WeatherModel implements WeatherModel {
  factory _WeatherModel(
      {final String? main,
      final String? description,
      final String? icon}) = _$_WeatherModel;

  factory _WeatherModel.fromJson(Map<String, dynamic> json) =
      _$_WeatherModel.fromJson;

  @override
  String? get main;
  @override
  String? get description;
  @override
  String? get icon;
  @override
  @JsonKey(ignore: true)
  _$$_WeatherModelCopyWith<_$_WeatherModel> get copyWith =>
      throw _privateConstructorUsedError;
}

MainModel _$MainModelFromJson(Map<String, dynamic> json) {
  return _MainModel.fromJson(json);
}

/// @nodoc
mixin _$MainModel {
  double? get temp => throw _privateConstructorUsedError;
  @JsonKey(name: "feels_like")
  double? get feelsLike => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MainModelCopyWith<MainModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MainModelCopyWith<$Res> {
  factory $MainModelCopyWith(MainModel value, $Res Function(MainModel) then) =
      _$MainModelCopyWithImpl<$Res, MainModel>;
  @useResult
  $Res call({double? temp, @JsonKey(name: "feels_like") double? feelsLike});
}

/// @nodoc
class _$MainModelCopyWithImpl<$Res, $Val extends MainModel>
    implements $MainModelCopyWith<$Res> {
  _$MainModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? temp = freezed,
    Object? feelsLike = freezed,
  }) {
    return _then(_value.copyWith(
      temp: freezed == temp
          ? _value.temp
          : temp // ignore: cast_nullable_to_non_nullable
              as double?,
      feelsLike: freezed == feelsLike
          ? _value.feelsLike
          : feelsLike // ignore: cast_nullable_to_non_nullable
              as double?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MainModelCopyWith<$Res> implements $MainModelCopyWith<$Res> {
  factory _$$_MainModelCopyWith(
          _$_MainModel value, $Res Function(_$_MainModel) then) =
      __$$_MainModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({double? temp, @JsonKey(name: "feels_like") double? feelsLike});
}

/// @nodoc
class __$$_MainModelCopyWithImpl<$Res>
    extends _$MainModelCopyWithImpl<$Res, _$_MainModel>
    implements _$$_MainModelCopyWith<$Res> {
  __$$_MainModelCopyWithImpl(
      _$_MainModel _value, $Res Function(_$_MainModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? temp = freezed,
    Object? feelsLike = freezed,
  }) {
    return _then(_$_MainModel(
      temp: freezed == temp
          ? _value.temp
          : temp // ignore: cast_nullable_to_non_nullable
              as double?,
      feelsLike: freezed == feelsLike
          ? _value.feelsLike
          : feelsLike // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MainModel implements _MainModel {
  _$_MainModel({this.temp, @JsonKey(name: "feels_like") this.feelsLike});

  factory _$_MainModel.fromJson(Map<String, dynamic> json) =>
      _$$_MainModelFromJson(json);

  @override
  final double? temp;
  @override
  @JsonKey(name: "feels_like")
  final double? feelsLike;

  @override
  String toString() {
    return 'MainModel(temp: $temp, feelsLike: $feelsLike)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MainModel &&
            (identical(other.temp, temp) || other.temp == temp) &&
            (identical(other.feelsLike, feelsLike) ||
                other.feelsLike == feelsLike));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, temp, feelsLike);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MainModelCopyWith<_$_MainModel> get copyWith =>
      __$$_MainModelCopyWithImpl<_$_MainModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MainModelToJson(
      this,
    );
  }
}

abstract class _MainModel implements MainModel {
  factory _MainModel(
      {final double? temp,
      @JsonKey(name: "feels_like") final double? feelsLike}) = _$_MainModel;

  factory _MainModel.fromJson(Map<String, dynamic> json) =
      _$_MainModel.fromJson;

  @override
  double? get temp;
  @override
  @JsonKey(name: "feels_like")
  double? get feelsLike;
  @override
  @JsonKey(ignore: true)
  _$$_MainModelCopyWith<_$_MainModel> get copyWith =>
      throw _privateConstructorUsedError;
}

WindModel _$WindModelFromJson(Map<String, dynamic> json) {
  return _WindModel.fromJson(json);
}

/// @nodoc
mixin _$WindModel {
  double? get speed => throw _privateConstructorUsedError;
  double? get deg => throw _privateConstructorUsedError;
  @JsonKey(name: "feels_like")
  String? get feelsLike => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WindModelCopyWith<WindModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WindModelCopyWith<$Res> {
  factory $WindModelCopyWith(WindModel value, $Res Function(WindModel) then) =
      _$WindModelCopyWithImpl<$Res, WindModel>;
  @useResult
  $Res call(
      {double? speed,
      double? deg,
      @JsonKey(name: "feels_like") String? feelsLike});
}

/// @nodoc
class _$WindModelCopyWithImpl<$Res, $Val extends WindModel>
    implements $WindModelCopyWith<$Res> {
  _$WindModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? speed = freezed,
    Object? deg = freezed,
    Object? feelsLike = freezed,
  }) {
    return _then(_value.copyWith(
      speed: freezed == speed
          ? _value.speed
          : speed // ignore: cast_nullable_to_non_nullable
              as double?,
      deg: freezed == deg
          ? _value.deg
          : deg // ignore: cast_nullable_to_non_nullable
              as double?,
      feelsLike: freezed == feelsLike
          ? _value.feelsLike
          : feelsLike // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_WindModelCopyWith<$Res> implements $WindModelCopyWith<$Res> {
  factory _$$_WindModelCopyWith(
          _$_WindModel value, $Res Function(_$_WindModel) then) =
      __$$_WindModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {double? speed,
      double? deg,
      @JsonKey(name: "feels_like") String? feelsLike});
}

/// @nodoc
class __$$_WindModelCopyWithImpl<$Res>
    extends _$WindModelCopyWithImpl<$Res, _$_WindModel>
    implements _$$_WindModelCopyWith<$Res> {
  __$$_WindModelCopyWithImpl(
      _$_WindModel _value, $Res Function(_$_WindModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? speed = freezed,
    Object? deg = freezed,
    Object? feelsLike = freezed,
  }) {
    return _then(_$_WindModel(
      speed: freezed == speed
          ? _value.speed
          : speed // ignore: cast_nullable_to_non_nullable
              as double?,
      deg: freezed == deg
          ? _value.deg
          : deg // ignore: cast_nullable_to_non_nullable
              as double?,
      feelsLike: freezed == feelsLike
          ? _value.feelsLike
          : feelsLike // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_WindModel implements _WindModel {
  _$_WindModel(
      {this.speed, this.deg, @JsonKey(name: "feels_like") this.feelsLike});

  factory _$_WindModel.fromJson(Map<String, dynamic> json) =>
      _$$_WindModelFromJson(json);

  @override
  final double? speed;
  @override
  final double? deg;
  @override
  @JsonKey(name: "feels_like")
  final String? feelsLike;

  @override
  String toString() {
    return 'WindModel(speed: $speed, deg: $deg, feelsLike: $feelsLike)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_WindModel &&
            (identical(other.speed, speed) || other.speed == speed) &&
            (identical(other.deg, deg) || other.deg == deg) &&
            (identical(other.feelsLike, feelsLike) ||
                other.feelsLike == feelsLike));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, speed, deg, feelsLike);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_WindModelCopyWith<_$_WindModel> get copyWith =>
      __$$_WindModelCopyWithImpl<_$_WindModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WindModelToJson(
      this,
    );
  }
}

abstract class _WindModel implements WindModel {
  factory _WindModel(
      {final double? speed,
      final double? deg,
      @JsonKey(name: "feels_like") final String? feelsLike}) = _$_WindModel;

  factory _WindModel.fromJson(Map<String, dynamic> json) =
      _$_WindModel.fromJson;

  @override
  double? get speed;
  @override
  double? get deg;
  @override
  @JsonKey(name: "feels_like")
  String? get feelsLike;
  @override
  @JsonKey(ignore: true)
  _$$_WindModelCopyWith<_$_WindModel> get copyWith =>
      throw _privateConstructorUsedError;
}
