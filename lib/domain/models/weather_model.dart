import 'package:freezed_annotation/freezed_annotation.dart';

part 'weather_model.freezed.dart';
part 'weather_model.g.dart';

@freezed
class CityWeatherModel with _$CityWeatherModel {
  factory CityWeatherModel({
    List<WeatherModel>? weather,
    MainModel? main,
    WindModel? wind,
    String? name,
  }) = _CityWeatherModel;

  factory CityWeatherModel.fromJson(Map<String, dynamic> json) => _$CityWeatherModelFromJson(json);
}

@freezed
class WeatherModel with _$WeatherModel {
  factory WeatherModel({
    String? main,
    String? description,
    String? icon,
  }) = _WeatherModel;

  factory WeatherModel.fromJson(Map<String, dynamic> json) => _$WeatherModelFromJson(json);
}

@freezed
class MainModel with _$MainModel {
  factory MainModel({
    double? temp,
    @JsonKey(name: "feels_like") double? feelsLike,
  }) = _MainModel;

  factory MainModel.fromJson(Map<String, dynamic> json) => _$MainModelFromJson(json);
}

@freezed
class WindModel with _$WindModel {
  factory WindModel({
    double? speed,
    double? deg,
    @JsonKey(name: "feels_like") String? feelsLike,
  }) = _WindModel;

  factory WindModel.fromJson(Map<String, dynamic> json) => _$WindModelFromJson(json);
}
